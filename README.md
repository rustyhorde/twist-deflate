# twist-deflate
An implementation of [RFC7692](tools.ietf.org/html/rfc7692) for the twist codec.
