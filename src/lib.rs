//! Per-message Deflate Extension
extern crate slog_term;

#[macro_use]
extern crate slog;
#[macro_use]
extern crate twist;

use slog::Logger;
use std::io;
use twist::{BaseFrame, FromHeader, IntoResponse, OpCode, PerMessage};

/// This extension needs to use the `rsv1` bit in a websocket frame.
const RSV1: u8 = 4;
/// The `Sec-WebSocket-Extensions` header prefix.
const SWE: &'static str = "Sec-WebSocket-Extensions: ";
/// The `permessage-deflate` header value.
const PMD: &'static str = "permessage-deflate";

/// Generate an `io::ErrorKind::Other` error with the given description.
fn other(desc: &str) -> io::Error {
    io::Error::new(io::ErrorKind::Other, desc)
}

/// The per-message deflate state.
pub struct Deflate {
    /// Is the Deflate extension enabled?
    enabled: bool,
    /// By including this extension parameter in an extension negotiation offer, a client prevents
    /// the peer server from using context takeover. Absence of this extension parameter in an
    /// extension negotiation offer indicates that the client can decompress a message that the
    /// server built using context takeover.
    _server_no_context_takeover: bool,
    /// By including this extension parameter in an extension negotiation offer, a client informs
    /// the peer server of a hint that even if the server doesn't include the
    /// `client_no_context_takeover` extension parameter in the corresponding extension negotiation
    /// response to the offer, the client is not going to use context takeover. By including the
    /// `client_no_context_takeover` extension parameter in an extension negotiation response, a
    /// server prevents the peer client from using context takeover. Absence of this extension
    /// parameter in an extension negotiation response indicates that the server can decompress
    /// messages built by the client using context takeover.
    _client_no_context_takeover: bool,
    /// By including this parameter in an extension negotiation offer, a client limits the LZ77
    /// sliding window size that the server will use to compress messages. A server declines an
    /// extension negotiation offer with this parameter if the server doesn't support it.
    /// Absence of this parameter in an extension negotiation offer indicates that the client can
    /// receive messages compressed using an LZ77 sliding window of up to 32,768 bytes. A server
    /// accepts an extension negotiation offer with this parameter by including the
    /// `server_max_window_bits` extension parameter in the extension negotiation response to send
    /// back to the client with the same or smaller value as the offer.
    server_max_window_bits: u8,
    /// The client max window bits.
    _client_max_window_bits: u8,
    /// slog stdout `Logger`
    stdout: Option<Logger>,
    /// slog stderr `Logger`
    stderr: Option<Logger>,
}

impl Deflate {
    /// Add a slog stdout `Logger` to this service.
    pub fn add_stdout(&mut self, stdout: Logger) -> &mut Deflate {
        let ps_stdout = stdout.new(o!("extension" => "permessage-deflate"));
        self.stdout = Some(ps_stdout);
        self
    }

    /// Add a slog stderr `Logger` to this service.
    pub fn add_stderr(&mut self, stderr: Logger) -> &mut Deflate {
        let ps_stderr = stderr.new(o!("extension" => "permessage-deflate"));
        self.stderr = Some(ps_stderr);
        self
    }
}

impl Default for Deflate {
    fn default() -> Deflate {
        Deflate {
            enabled: false,
            _server_no_context_takeover: false,
            _client_no_context_takeover: true,
            server_max_window_bits: 15,
            _client_max_window_bits: 15,
            stdout: None,
            stderr: None,
        }
    }
}

impl FromHeader for Deflate {
    fn init(&mut self, request: &str) -> Result<(), io::Error> {
        try_trace!(self.stdout, "init Deflate with header '{}'", request);

        if request.contains(PMD) {
            self.enabled = true;
            try_trace!(self.stdout, "permessage deflate is enabled");

            let blah: Vec<&str> =
                request.split(',').take_while(|s| s.starts_with("permessage-deflate")).collect();
            for pmd in blah {
                try_trace!(self.stdout, "{}", pmd);
            }
            self.server_max_window_bits = 8;
        } else {
            try_trace!(self.stdout, "permessage deflate is disabled");
        }
        Ok(())
    }
}

impl IntoResponse for Deflate {
    fn response(&self) -> Option<String> {
        if self.enabled {
            let mut resp = String::new();
            resp.push_str(SWE);
            resp.push_str(PMD);
            Some(resp)
        } else {
            None
        }
    }
}

impl PerMessage for Deflate {
    fn enabled(&self) -> bool {
        self.enabled
    }

    fn reserve_rsv(&self, current_rsv: u8) -> Result<u8, io::Error> {
        if self.enabled {
            if current_rsv & RSV1 == 0 {
                Ok(current_rsv | RSV1)
            } else {
                Err(other("rsv1 bit already reserved"))
            }
        } else {
            Ok(0)
        }
    }

    fn decode(&self, message: &mut BaseFrame) -> Result<(), io::Error> {
        if self.enabled {
            let opcode = message.opcode();
            if (opcode == OpCode::Text || opcode == OpCode::Binary) && message.fin() {
                try_trace!(self.stdout, "Decompressing Frame");
            }
        }
        Ok(())
    }

    fn encode(&self, message: &mut BaseFrame) -> Result<(), io::Error> {
        if self.enabled {
            let opcode = message.opcode();
            if (opcode == OpCode::Text || opcode == OpCode::Binary) && message.fin() {
                try_trace!(self.stdout, "Compressing Frame");
            }
        }
        Ok(())
    }
}


#[test]
fn it_works() {}
